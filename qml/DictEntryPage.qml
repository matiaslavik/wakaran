/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: dict_entry_page
    anchors.fill: parent

    header: RowLayout {

        Button {
            id: backButton
            anchors.left: parent.left
            text: qsTr("←")
            font.pixelSize: FontUtils.sizeToPixels("x-large")
            padding: units.gu(1)
            onClicked: dictEntryPageViewModel.goBack()
        }

        Text {
            id: headerText
            anchors.left: backButton.right
            anchors.leftMargin: units.gu(1)
            text: "Dictionary entry"
            font.pixelSize: FontUtils.sizeToPixels("x-large")
        }
    }

    Rectangle {
        id: background
        color: "#2b93e6"
        anchors.fill: parent

        Rectangle {
            id: listBackground
            color: "#ffffff"
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: units.gu(4)
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
        }

        Text {
            id: textHiragana
            anchors.top: listBackground.top
            anchors.left: listBackground.left
            anchors.topMargin: units.gu(2)
            anchors.leftMargin: units.gu(2)
            text: dictEntryPageViewModel.hiraganaText
            font.pixelSize: FontUtils.sizeToPixels("large")
        }

        Text {
            id: textRomaji
            anchors.top: textHiragana.bottom
            anchors.left: listBackground.left
            anchors.leftMargin: units.gu(2)
            text: dictEntryPageViewModel.romajiText
            font.pixelSize: FontUtils.sizeToPixels("large")
            color: "blue"
        }

        Text {
            id: textSource
            anchors.top: textRomaji.bottom
            anchors.left: listBackground.left
            anchors.leftMargin: units.gu(2)
            text: dictEntryPageViewModel.sourceText
            font.pixelSize: FontUtils.modularScale("x-large") * units.dp(20)
        }

        ListView {
            id: transList
            anchors.top: textSource.bottom
            anchors.bottom: favButton.top
            anchors.left: listBackground.left
            anchors.right: listBackground.right
            anchors.leftMargin: units.gu(2)
            anchors.bottomMargin: units.gu(7)

            model: dictEntryPageViewModel.translations
            delegate: Text {
                anchors.left: parent.left
                anchors.right: parent.right
                text: modelData
                wrapMode: Text.WordWrap
                font.pixelSize: FontUtils.sizeToPixels("large")
            }
        }

        MouseArea {
            id: favButton
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.bottomMargin: units.gu(4)
            anchors.leftMargin: units.gu(2)
            width: units.gu(5)
            height: units.gu(5)
            onClicked: {
                dictEntryPageViewModel.toggleFavourited()
            }

            Image {
                anchors.fill: parent
                source: "star-white.svg"
                visible: !dictEntryPageViewModel.favourited
            }
            Image {
                anchors.fill: parent
                source: "star-yellow.svg"
                visible: dictEntryPageViewModel.favourited
            }
        }
    }

}
