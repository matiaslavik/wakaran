/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Example 1.0

Page {
    id: main_page
    anchors.fill: parent

    Drawer {
        id: drawer
        y: view.y
        width: window.width * 0.5
        height: window.height - view.y

        Button {
            id: drawerSearchButton
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: units.gu(4)
            anchors.leftMargin: units.gu(1)
            text: "Search page"
            font.pixelSize: FontUtils.sizeToPixels("large")
            onClicked: {
                view.currentIndex = 0
                drawer.close()
            }
        }
        Button {
            id: drawerHistButton
            anchors.top: drawerSearchButton.bottom
            anchors.left: drawerSearchButton.left
            anchors.topMargin: units.gu(1)
            text: "History"
            font.pixelSize: FontUtils.sizeToPixels("large")
            onClicked: {
                view.currentIndex = 1
                drawer.close()
            }
        }
        Button {
            id: drawerCollButton
            anchors.top: drawerHistButton.bottom
            anchors.left: drawerHistButton.left
            anchors.topMargin: units.gu(1)
            text: "Favourites"
            font.pixelSize: FontUtils.sizeToPixels("large")
            onClicked: {
                view.currentIndex = 2
                drawer.close()
            }
        }
        Button {
            id: drawerIssueButton
            anchors.bottom: parent.bottom
            anchors.left: drawerHistButton.left
            anchors.bottomMargin: units.gu(1)
            text: "Report an issue"
            font.pixelSize: FontUtils.sizeToPixels("large")
            onClicked: {
                Qt.openUrlExternally("https://codeberg.org/matiaslavik/wakaran/issues");
                drawer.close()
            }
        }

        Button {
            id: aboutButton
            anchors.bottom: drawerIssueButton.top
            anchors.left: drawerHistButton.left
            anchors.bottomMargin: units.gu(1)
            text: "About"
            font.pixelSize: FontUtils.sizeToPixels("large")
            onClicked: {
                mainViewModel.pageIndex = 2;
                drawer.close()
            }
        }
    }

    Rectangle {
        id: background
        color: "#2b93e6"
        anchors.fill: parent
    }

    Image {
        id: drawerButton
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: searchButton.bottom
        anchors.topMargin: units.gu(4)
        anchors.leftMargin: units.gu(1)
        width: height
        source: "menu.svg"
        MouseArea {
            anchors.fill: parent
            onClicked: {
               drawer.open()
            }
        }
    }

    Rectangle {
        id: searchTextBackground
        anchors.left: drawerButton.right
        anchors.right: searchButton.left
        anchors.top: parent.top
        anchors.bottom: searchButton.bottom
        anchors.topMargin: units.gu(4)
        anchors.rightMargin: units.gu(1)
        anchors.leftMargin: 10
        color: "white"
    }

    TextEdit {
        id: searchText
        anchors.fill: searchTextBackground
        anchors.leftMargin: units.gu(1)
        font.pixelSize: FontUtils.sizeToPixels("large")
        verticalAlignment: TextEdit.AlignVCenter
        Keys.onReturnPressed: {
            searchPageModel.search(searchText.text)
            view.setCurrentIndex(0)
        }
    }

    Button {
        id: searchButton
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: units.gu(4)
        anchors.rightMargin: units.gu(1)
        padding: units.gu(1)
        text: qsTr("Search")
        font.pixelSize: FontUtils.sizeToPixels("large")
        onClicked: {
            searchPageModel.search(searchText.text)
            view.setCurrentIndex(0)
        }
    }

    SwipeView {
        id: view
        anchors.top: searchText.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: units.gu(2)

        currentIndex: mainViewModel.viewIndex

        Item {
            SearchPage{
            }
        }
        Item {
            HistoryPage{
            }
        }
        Item {
            CollectionsPage{
            }
        }
    }

    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
