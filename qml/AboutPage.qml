/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: about_page
    anchors.fill: parent

    header: RowLayout {

        Rectangle {
            id: headerBackground
            color: "#2b93e6"
            anchors.fill: parent
        }

        Button {
            id: backButton
            text: qsTr("←")
            anchors.left: parent.left
            font.pixelSize: FontUtils.sizeToPixels("x-large")
            padding: units.gu(1)
            onClicked: {
                mainViewModel.pageIndex = 0
            }
        }

        Text {
            id: headerText
            anchors.left: backButton.right
            anchors.leftMargin: units.gu(1)
            text: "About Wakaran"
            font.pixelSize: FontUtils.sizeToPixels("x-large")
        }
    }

    Rectangle {
        id: background
        color: "white"
        anchors.fill: parent
    }

    Image {
        id: logoImage
        anchors.top: parent.top
        anchors.topMargin: units.gu(2)
        width: units.gu(10)
        height: units.gu(10)
        source: "wakaran.png"
    }

    Text {
        id: wakaranText
        anchors.top: logoImage.bottom
        anchors.left: background.left
        anchors.right: background.right
        anchors.topMargin: units.gu(2)
        anchors.leftMargin: units.gu(2)
        anchors.rightMargin: units.gu(2)
        wrapMode: Text.WordWrap
        text: "Wakaran was made by Matias Lavik. Source code is available <a href='https://codeberg.org/matiaslavik/wakaran'>here</a>."
        font.pixelSize: FontUtils.sizeToPixels("large")
        onLinkActivated: Qt.openUrlExternally("https://codeberg.org/matiaslavik/wakaran")
    }

    Text {
        id: jmdictText
        anchors.top: wakaranText.bottom
        anchors.left: background.left
        anchors.right: background.right
        anchors.topMargin: units.gu(3)
        anchors.leftMargin: units.gu(2)
        anchors.rightMargin: units.gu(2)
        wrapMode: Text.WordWrap
        text: "Wakaran uses data from the <a href='http://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project'>JMdict/EDICT  project</a>."
        font.pixelSize: FontUtils.sizeToPixels("large")
        onLinkActivated: Qt.openUrlExternally("http://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project")
    }
}
