/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Page {
    id: ssearch_page
    anchors.fill: parent

    header: PageHeader {
        id: header
        title: i18n.tr('Search results')
    }

    Rectangle {
        id: listBackground
        color: "#ffffff"
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        ListView {
            id: searchResultList
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: units.gu(1)
            spacing: units.gu(1)
            clip:true
            onCountChanged: {
                searchResultList.positionViewAtBeginning()
            }

            model: searchResultListModel
            delegate: Item {
                width: parent.width
                height: childrenRect.height

                Text {
                    id: kanjiText
                    font.pixelSize: FontUtils.sizeToPixels("large")
                    text: edit.sourceText
                }
                Text {
                    id: kanaText
                    anchors.left: kanjiText.right
                    font.pixelSize: FontUtils.sizeToPixels("large")
                    color: "blue"
                    text: "(" + edit.hiraganaText + ")"
                }

                Text {
                    id: romajiText
                    anchors.top: kanaText.bottom
                    font.pixelSize: FontUtils.sizeToPixels("medium")
                    color: "red"
                    text: "[" + edit.romajiText + "] "
                }
                Text {
                    id: transText
                    anchors.top: kanaText.bottom
                    anchors.left: romajiText.right
                    font.pixelSize: FontUtils.sizeToPixels("medium")
                    text: edit.translations[0]
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: { mainViewModel.showDictionaryEntry(edit) }
                }
            }
        }
    }
}
