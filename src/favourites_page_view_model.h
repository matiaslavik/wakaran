﻿#ifndef FAVOURITESPAGEVIEWMODEL_H
#define FAVOURITESPAGEVIEWMODEL_H

#include <QObject>
#include <QString>
#include <QList>
#include "dictionary_entry.h"
#include <QAbstractItemModel>

class FavouritesPageViewModel : public QObject
{
    Q_OBJECT

public:
    explicit FavouritesPageViewModel()
    {
    }

    virtual ~FavouritesPageViewModel()
    { }

    void addEntry(DictionaryEntry* entry);
    void removeEntry(DictionaryEntry* entry);
    void initialise();

private:
    QList<DictionaryEntry*> mDictionaryEntries;
    bool mHasInitialised = false;

    void refreshList();
};

#endif
