﻿#include "dictionary_entry.h"

DictionaryEntry::DictionaryEntry()
{

}

DictionaryEntry::DictionaryEntry(int id, QString source, QString hiragana, QString romaji, QStringList translations, int priority)
{
    mID = id;
    mSourceText = source;
    mHiraganaText = hiragana;
    mRomajiText = romaji;
    mTranslations = translations;
    mPriority = priority;

    emit entryChangedEvent();
}
