/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USERDATABASE_H
#define USERDATABASE_H

#include "dictionary_database.h"

/*
 * User database, for storing user-specific data.
 * Inherits from DictionaryDatabase, and contains a copy of all the dictionary entries.
 */
class UserDatabase : public DictionaryDatabase
{
public:
    virtual bool loadDatabase(QString filePath, QString dbname) override;
    virtual void clearDatabase() override;

    /* Adds the specified dictionary entry to the view history. */
    void addHistoryEntry(DictionaryEntry* entry);

    /* Adds the specified dictionary entry to the favourite list. */
    void addToFavourites(DictionaryEntry* entry);

    /* Removes the specified dictionary entry from the favourite list. */
    void removeFromFavourites(DictionaryEntry* entry);

    /* Checks if a dictionary entry has been added as a favourite. */
    bool isFavourited(DictionaryEntry* entry);

    /* Constructs DictionaryEntry instances for all favourited dictionary entries. */
    void getFavourites(QList<DictionaryEntry*>& outDictEntries);

    /* Construct DictionaryEntry instances from the view history. */
    void loadHistory(QList<DictionaryEntry*>& outDictEntries);

    /* Copy dictionary entries from another database. */
    bool updateDictionaryEntriesFromDB(DictionaryDatabase* sourceDB);
};
#endif
