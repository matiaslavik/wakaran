﻿#ifndef DICTENTRYPAGEVIEWMODEL_H
#define DICTENTRYPAGEVIEWMODEL_H

#include <QObject>
#include <QString>
#include <QList>
#include "dictionary_entry.h"

/*
 * View model for the dictionary entry page.
 * This is the page where you can view info about a dictionary entry,
 *  such as reading, writing and translations.
 */
class DictEntryPageViewModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString sourceText READ getSourceText NOTIFY sourceTextChangedEvent)
    Q_PROPERTY(QString hiraganaText READ getHiraganaText NOTIFY hiraganaTextChangedEvent)
    Q_PROPERTY(QString romajiText READ getRomajiText NOTIFY romajiTextChangedEvent)
    Q_PROPERTY(QStringList translations READ getTranslations NOTIFY translationsChangedEvent)
    Q_PROPERTY(bool favourited READ getFavourited NOTIFY favouritedChangedEvent)

private:
    DictionaryEntry* mSearchResult = nullptr;
    bool mFavourited = false;

public:
    DictEntryPageViewModel() {}

    Q_INVOKABLE void goBack();
    Q_INVOKABLE void toggleFavourited();

    QString getSourceText() const;
    QString getHiraganaText() const;
    QString getRomajiText() const;
    QStringList getTranslations() const;
    bool getFavourited() const;

    void setDictEntry(DictionaryEntry* searchResult);

signals:
    void sourceTextChangedEvent(QString arg);
    void hiraganaTextChangedEvent(QString arg);
    void romajiTextChangedEvent(QString arg);
    void translationsChangedEvent();
    void favouritedChangedEvent(bool arg);
};

#endif
