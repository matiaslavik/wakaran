/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dictionary_database.h"
#include <QSqlQuery>
#include <QSqlResult>
#include <QTextStream>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include "app.h"
#include <QQmlEngine>

bool DictionaryDatabase::loadDatabase(QString filePath, QString dbname)
{
    mDatabaseFilepath = filePath;
    qDebug() << "Loading database from: " << mDatabaseFilepath;

    mSqlDatabase = QSqlDatabase::addDatabase("QSQLITE", dbname);
    mSqlDatabase.setDatabaseName(mDatabaseFilepath);
    if(mSqlDatabase.open())
    {
        qDebug() << "Database open";
        qDebug() << "Num tables: " << mSqlDatabase.tables().count();

        if(!mSqlDatabase.tables().contains("history"))
        {
            QSqlQuery createTableQuery(mSqlDatabase);
            createTableQuery.exec("create table dict_entries (id int primary key, kanji_string varchar(255), kana_string varchar(255), romaji_string varchar(255), trans_string varchar(255))");
        }

        return true;
    }
    else
    {
        qDebug() << "Failed to load database!";
        return false;
    }
}

void DictionaryDatabase::clearDatabase()
{
    QSqlQuery deleteQuery(mSqlDatabase);
    deleteQuery.exec("delete from dict_entries");
}

void DictionaryDatabase::importDictionaryEntries(QList<DictionaryEntry*> dictEntries)
{
    for(DictionaryEntry* dictEntry : dictEntries)
    {
        QString kanjiString;
        QString kanaString;
        QString romajiString;
        QString transString;
        QTextStream transStream(&transString);

        kanjiString = dictEntry->getSourceText();
        kanaString = dictEntry->getHiraganaText();
        romajiString = dictEntry->getRomajiText();

        QStringList entryTranslations = dictEntry->getTranslations();
        for(int iTrans = 0; iTrans < entryTranslations.count(); iTrans++)
        {
            if(iTrans == 0)
                transStream << entryTranslations[iTrans];
            else
                transStream << ";" << entryTranslations[iTrans];
        }

        QSqlQuery insertQuery(mSqlDatabase);
        insertQuery.prepare("insert into dict_entries values(?,?,?,?,?)");
        insertQuery.addBindValue(dictEntry->getID());
        insertQuery.addBindValue(kanjiString);
        insertQuery.addBindValue(kanaString);
        insertQuery.addBindValue(romajiString);
        insertQuery.addBindValue(transString);
        insertQuery.exec();
    }
}

void DictionaryDatabase::constructDictionaryEntries(QList<DictionaryEntry*>& outDictEntries)
{
    QSqlQuery entriesQuery(mSqlDatabase);
    entriesQuery.exec("select id, kanji_string, kana_string, romaji_string, trans_string from dict_entries");

    int i = 1000000; // TODO: Store priority in db?
    while (entriesQuery.next())
    {
        int id = entriesQuery.value(0).toInt();
        QString kanjiString = entriesQuery.value(1).toString();
        QString kanaString = entriesQuery.value(2).toString();
        QString romajiString = entriesQuery.value(3).toString();
        QString transString = entriesQuery.value(4).toString();
        QStringList transList = transString.split(';');

        DictionaryEntry* dictEntry = new DictionaryEntry(id, kanjiString, kanaString, romajiString, transList, i);
        outDictEntries.push_back(dictEntry);
        i--;
    }
}

void DictionaryDatabase::searchDictEntries(QString searchString, QList<DictionaryEntry*>& outResults)
{
    QString likeString = "%" + searchString + "%";

    QSqlQuery searchQuery(mSqlDatabase);
    searchQuery.prepare("select id, kanji_string, kana_string, romaji_string, trans_string from dict_entries where (romaji_string like ? or kana_string like ? or kanji_string like ? or trans_string like ?) limit 50");
    searchQuery.addBindValue(likeString);
    searchQuery.addBindValue(likeString);
    searchQuery.addBindValue(likeString);
    searchQuery.addBindValue(likeString);
    searchQuery.exec();

    int i = 1000000; // TODO: Store priority in db?
    while (searchQuery.next())
    {
        int id = searchQuery.value(0).toInt();
        QString kanjiString = searchQuery.value(1).toString();
        QString kanaString = searchQuery.value(2).toString();
        QString romajiString = searchQuery.value(3).toString();
        QString transString = searchQuery.value(4).toString();
        QStringList transList = transString.split(';');

        DictionaryEntry* dictEntry = new DictionaryEntry(id, kanjiString, kanaString, romajiString, transList, i);
        outResults.push_back(dictEntry);
        i--;
    }
}
