﻿#include "search_page_view_model.h"
#include "app.h"
#include "search_result_list_model.h"
#include "user_database.h"

void SearchPageViewModel::search(QString searchString)
{
    GApp->mSearchResultListModel->clearSearchResults();

    QList<DictionaryEntry*> results;
    GApp->mUserDatabase->searchDictEntries(searchString, results);
    for(DictionaryEntry* item : results)
        GApp->mSearchResultListModel->addSearchResult(item);
}
