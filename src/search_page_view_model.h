﻿#ifndef SEARCHPAGEVIEWMODEL_H
#define SEARCHPAGEVIEWMODEL_H

#include <QObject>
#include <QString>
#include <QList>
#include "dictionary_entry.h"
#include <QAbstractItemModel>

class SearchPageViewModel : public QObject
{
    Q_OBJECT

public:
    explicit SearchPageViewModel()
    {
    }

    virtual ~SearchPageViewModel()
    { }

    Q_INVOKABLE void search(QString searchString);

//private:
public:
    QList<DictionaryEntry*> mDictionaryEntries; // TODO
};

#endif // SEARCHPAGEVIEWMODEL_H
