﻿#include "favourites_page_view_model.h"
#include "app.h"
#include "search_result_list_model.h"
#include "user_database.h"

void FavouritesPageViewModel::addEntry(DictionaryEntry* entry)
{
    if(!mHasInitialised)
        initialise();

    mDictionaryEntries.removeAll(entry);

    mDictionaryEntries.push_back(entry);

    refreshList();
}

void FavouritesPageViewModel::removeEntry(DictionaryEntry* entry)
{
    mDictionaryEntries.removeAll(entry);

    refreshList();
}

void FavouritesPageViewModel::initialise()
{
    if(mHasInitialised)
        return;

    mDictionaryEntries.clear();

    if(GApp->mUserDatabase != nullptr)
        GApp->mUserDatabase->getFavourites(mDictionaryEntries);

    refreshList();

    mHasInitialised = true;
}

void FavouritesPageViewModel::refreshList()
{
    GApp->mFavouritesListModel->clearSearchResults();

    // Add in reverse order
    for(int i = mDictionaryEntries.size() - 1; i >= 0; i--)
        GApp->mFavouritesListModel->addSearchResult(mDictionaryEntries[i]);
}
