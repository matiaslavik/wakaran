/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xml_dict_loader.h"
#include "search_result_list_model.h"
#include "app.h"
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QFile>
#include "kana_utils.h"

XmlDictLoader::XmlDictLoader()
{
    mPriorityValues.emplace("ichi1", 16);
    mPriorityValues.emplace("news1", 16);
    mPriorityValues.emplace("spec1", 16);
    mPriorityValues.emplace("gai1", 16);
    mPriorityValues.emplace("ichi2", 4);
    mPriorityValues.emplace("news2", 4);
    mPriorityValues.emplace("spec2", 4);
    mPriorityValues.emplace("gai2", 4);
}

XmlDictLoader::~XmlDictLoader()
{

}

int XmlDictLoader::parsePriority(QString priString)
{
    auto it = mPriorityValues.find(priString.toStdString());
    if(it != mPriorityValues.end())
        return it->second;
    else
        return 1;
}

bool XmlDictLoader::parseKanji(QDomElement elem, QStringList& outList, int& priority)
{
    QDomElement child = elem.firstChild().toElement();
    while (!child.isNull())
    {
        if(child.tagName() == "keb")
        {
            outList.push_back(child.text());
        }
        else if(child.tagName() == "ke_pri")
        {
            priority += parsePriority(child.text());
        }

        child = child.nextSibling().toElement();
    }
}

bool XmlDictLoader::parseReading(QDomElement elem, QStringList& outList, int& priority)
{
    QDomElement child = elem.firstChild().toElement();
    while (!child.isNull())
    {
        if(child.tagName() == "reb")
        {
            outList.push_back(child.text());
        }
        else if(child.tagName() == "re_pri")
        {
            priority += parsePriority(child.text());
        }

        child = child.nextSibling().toElement();
    }
}

bool XmlDictLoader::parseTranslation(QDomElement elem, QStringList& outList)
{
    QDomElement child = elem.firstChild().toElement();
    while (!child.isNull())
    {
        if(child.tagName() == "gloss")
        {
            outList.push_back(child.text());
        }

        child = child.nextSibling().toElement();
    }
}

DictionaryEntry* XmlDictLoader::parseEntry(QDomElement entry)
{
    QStringList kanjiList;
    QStringList readingList;
    QStringList transList;
    int priority = 0;
    int id = -1;

    QDomElement child = entry.firstChild().toElement();
    while (!child.isNull())
    {
        if(child.tagName() == "ent_seq")
        {
            id = child.text().toInt();
        }
        else if(child.tagName() == "k_ele")
        {
            parseKanji(child, kanjiList, priority);
        }
        else if(child.tagName() == "r_ele")
        {
            parseReading(child, readingList, priority);
        }
        else if(child.tagName() == "sense")
        {
            parseTranslation(child, transList);
        }

        child = child.nextSibling().toElement();
    }

    if(id != -1 && (!kanjiList.empty() || !readingList.empty()) && !transList.empty())
    {
        QStringList romajiList;
        for(QString str : readingList)
            romajiList.push_back(KanaUtils::kanaToRomaji(str));

        QString kanjiString = kanjiList.count() > 0 ? kanjiList[0] : ""; // TODO: add all writings
        QString kanaString = readingList.count() > 0 ? readingList[0] : ""; // TODO: add all readings
        QString romajiString = romajiList.count() > 0 ? romajiList[0] : ""; // TODO: add all readings

        DictionaryEntry* item = new DictionaryEntry(id, kanjiString, kanaString, romajiString, transList, priority);
        return item;
    }
    return nullptr;
}

void XmlDictLoader::laodFromXML(QString xmlPath, QList<DictionaryEntry*>& outDictEntries)
{
    QDomDocument xmlDoc;
    QFile xmlFile(xmlPath);
    if(xmlFile.open(QIODevice::ReadOnly))
    {
        xmlDoc.setContent(&xmlFile);
        xmlFile.close();

        QDomElement root = xmlDoc.documentElement();
        qDebug() << root.tagName();
        qDebug() <<  root.childNodes().count();

        QDomElement child = root.firstChild().toElement();

        while (!child.isNull())
        {
            if(child.tagName() == "entry")
            {
               DictionaryEntry* dictEntry = parseEntry(child);
               if(dictEntry != nullptr)
                   outDictEntries.push_back(dictEntry);
            }

            child = child.nextSibling().toElement();
        }

        // Sort entries by priority (most common entries first)
        auto sorter = [](const DictionaryEntry* a, const DictionaryEntry* b) -> bool
        {
            return a->getPriority() > b->getPriority();
        };
        qSort(outDictEntries.begin(), outDictEntries.end(), sorter);

        xmlDoc.clear();
    }
    else
        qDebug() << "XML FAILED TO OPEN";
}
