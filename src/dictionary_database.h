/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DICTIONARYDATABASE_H
#define DICTIONARYDATABASE_H

#include "dictionary_entry.h"
#include <QList>
#include <qstring.h>
#include <QSqlDatabase>

/*
 * Dictionary database.
 * Sql database for looking up dictionary entries.
 */
class DictionaryDatabase
{
protected:
    QSqlDatabase mSqlDatabase;

    QString mDatabaseFilepath;

public:
    virtual bool loadDatabase(QString filePath, QString dbname);
    virtual void clearDatabase();

    /* Import a list of dictionary entries into the database. */
    void importDictionaryEntries(QList<DictionaryEntry*> dictEntries);

    /* Construct DictionaryEntry instances for all entries in the database. */
    void constructDictionaryEntries(QList<DictionaryEntry*>& outDictEntries);

    /* Searches for a dictionary entry matching the specified search string. */
    virtual void searchDictEntries(QString searchString, QList<DictionaryEntry*>& outResults);

    QSqlDatabase& getSqlDatabase() { return mSqlDatabase; }
};
#endif
