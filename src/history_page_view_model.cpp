﻿#include "history_page_view_model.h"
#include "app.h"
#include "search_result_list_model.h"
#include "user_database.h"

void HistoryPageViewModel::addEntry(DictionaryEntry* entry)
{
    if(!mHasInitialised)
        initialise();

    mDictionaryEntries.removeAll(entry);

    mDictionaryEntries.push_back(entry);

    refreshList();
}

void HistoryPageViewModel::initialise()
{
    if(mHasInitialised)
        return;

    mDictionaryEntries.clear();

    if(GApp->mUserDatabase != nullptr)
        GApp->mUserDatabase->loadHistory(mDictionaryEntries);

    refreshList();

    mHasInitialised = true;
}

void HistoryPageViewModel::refreshList()
{
    GApp->mHistoryListModel->clearSearchResults();

    // Add in reverse order
    for(int i = mDictionaryEntries.size() - 1; i >= 0; i--)
        GApp->mHistoryListModel->addSearchResult(mDictionaryEntries[i]);
}
