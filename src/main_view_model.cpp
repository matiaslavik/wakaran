#include "main_view_model.h"
#include "app.h"
#include "dict_entry_page_view_model.h"
#include "user_database.h"
#include <QGuiApplication>

void MainViewModel::showDictionaryEntry(DictionaryEntry* item)
{
    if(item == nullptr)
    {
        qDebug() << "showDictionaryEntry called with null item!";
        return;
    }

    QGuiApplication::inputMethod()->setVisible(false);

    setPageIndex(1);
    GApp->mDictEntryPageViewModel->setDictEntry(item);

    if(GApp->mUserDatabase != nullptr)
        GApp->mUserDatabase->addHistoryEntry(item);
    GApp->mHistoryPageViewModel->addEntry(item);

    qDebug() << item->getRomajiText();
}

int MainViewModel::getPageIndex() const
{
    return mPageIndex;
}

void MainViewModel::setPageIndex(int arg)
{
    if (getPageIndex() != arg)
    {
        qDebug() << "set tab index " << arg;
        mPageIndex = arg;
        pageIndexChangedEvent(mPageIndex);
    }
}

int MainViewModel::getViewIndex() const
{
    return mViewIndex;
}

void MainViewModel::setViewIndex(int arg)
{
    if (getViewIndex() != arg)
    {
        qDebug() << "set view index " << arg;
        mViewIndex = arg;
        viewIndexChangedEvent(arg);
    }
}
