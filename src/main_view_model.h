#ifndef MAINVIEWMODEL_H
#define MAINVIEWMODEL_H

#include <QObject>
#include <QDebug>
#include "dictionary_entry.h"

class MainViewModel : public QObject
{
    Q_OBJECT

private:
    int mPageIndex = 0;
    int mViewIndex = 1; // starts on history page

    Q_PROPERTY (int pageIndex READ getPageIndex WRITE setPageIndex NOTIFY pageIndexChangedEvent)
    Q_PROPERTY (int viewIndex READ getViewIndex WRITE setViewIndex NOTIFY viewIndexChangedEvent)

public:
    explicit MainViewModel()
    { }

    virtual ~MainViewModel()
    { }

    Q_INVOKABLE void showDictionaryEntry(DictionaryEntry* item);

    int getPageIndex() const;
    int getViewIndex() const;

public slots:
    void setPageIndex(int arg);
    void setViewIndex(int arg);

signals:
    void pageIndexChangedEvent(int arg);
    void viewIndexChangedEvent(int arg);
};

#endif // MAINVIEWMODEL_H
