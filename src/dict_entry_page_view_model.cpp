﻿#include "dict_entry_page_view_model.h"
#include "app.h"
#include "main_view_model.h"
#include "user_database.h"

void DictEntryPageViewModel::goBack()
{
    GApp->mMainViewModel->setPageIndex(0);
}

QString DictEntryPageViewModel::getSourceText() const
{
    if(mSearchResult != nullptr)
        return mSearchResult->getSourceText();
    else
        return "";
}

QString DictEntryPageViewModel::getHiraganaText() const
{
    if(mSearchResult != nullptr)
        return mSearchResult->getHiraganaText();
    else
        return "";
}

QString DictEntryPageViewModel::getRomajiText() const
{
    if(mSearchResult != nullptr)
        return mSearchResult->getRomajiText();
    else
        return "";
}

QStringList DictEntryPageViewModel::getTranslations() const
{
    if(mSearchResult != nullptr)
        return mSearchResult->getTranslations();
    else
        return QStringList();
}

bool DictEntryPageViewModel::getFavourited() const
{
    return mFavourited;
}

void DictEntryPageViewModel::setDictEntry(DictionaryEntry* searchResult)
{
    mSearchResult = searchResult;

    if(GApp->mUserDatabase != nullptr)
        mFavourited = GApp->mUserDatabase->isFavourited(searchResult);

    emit sourceTextChangedEvent(getSourceText());
    emit hiraganaTextChangedEvent(getHiraganaText());
    emit romajiTextChangedEvent(getRomajiText());
    emit translationsChangedEvent();
    emit favouritedChangedEvent(mFavourited);
}

void DictEntryPageViewModel::toggleFavourited()
{
    if(GApp->mUserDatabase != nullptr)
    {
        if(mFavourited)
        {
            GApp->mUserDatabase->removeFromFavourites(mSearchResult);
            GApp->mFavouritesPageViewModel->removeEntry(mSearchResult);
        }
        else
        {
            GApp->mUserDatabase->addToFavourites(mSearchResult);
            GApp->mFavouritesPageViewModel->addEntry(mSearchResult);
        }
        mFavourited = !mFavourited;
        emit favouritedChangedEvent(mFavourited);
    }
}
