﻿#ifndef HISTORYPAGEVIEWMODEL_H
#define HISTORYPAGEVIEWMODEL_H

#include <QObject>
#include <QString>
#include <QList>
#include "dictionary_entry.h"
#include <QAbstractItemModel>

class HistoryPageViewModel : public QObject
{
    Q_OBJECT

public:
    explicit HistoryPageViewModel()
    {
    }

    virtual ~HistoryPageViewModel()
    { }

    void addEntry(DictionaryEntry* entry);
    void initialise();

private:
    QList<DictionaryEntry*> mDictionaryEntries;
    bool mHasInitialised = false;

    void refreshList();
};

#endif
