/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "user_database.h"
#include <QSqlQuery>
#include <QSqlResult>
#include <QTextStream>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include "app.h"
#include <QQmlEngine>
#include <QDateTime>
#include <unordered_map>
#include <vector>

bool UserDatabase::loadDatabase(QString filePath, QString dbname)
{
    qDebug() << "Loading user database from: " << filePath;

    bool baseLoadRes = DictionaryDatabase::loadDatabase(filePath, dbname);
    if(!baseLoadRes)
        return false;

    if(!mSqlDatabase.tables().contains("versions"))
    {
        qDebug() << "Creating versions table";
        QSqlQuery createTableQuery(mSqlDatabase);
        createTableQuery.exec("create table versions (version_id int unique)");
    }

    if(!mSqlDatabase.tables().contains("history"))
    {
        qDebug() << "Creating history table";
        QSqlQuery createTableQuery(mSqlDatabase);
        createTableQuery.exec("create table history (id integer primary key autoincrement, dict_entry integer, time timestamp)");
    }

    if(!mSqlDatabase.tables().contains("collections"))
    {
        qDebug() << "Creating collections table";
        QSqlQuery createTableQuery(mSqlDatabase);
        createTableQuery.exec("create table collections (id integer primary key autoincrement, name varchar(255))");

        QSqlQuery favCollQuery(mSqlDatabase);
        favCollQuery.prepare("insert into collections(name) values(?)");
        favCollQuery.addBindValue("Favourites");
        favCollQuery.exec();
    }

    if(!mSqlDatabase.tables().contains("saved_items"))
    {
        qDebug() << "Creating saved_items table";
        QSqlQuery createTableQuery(mSqlDatabase);
        createTableQuery.exec("create table saved_items (id integer primary key autoincrement, dict_entry int, coll_id int, time timestamp)");
    }

    return true;
}

void UserDatabase::clearDatabase()
{
    DictionaryDatabase::clearDatabase();
}

bool UserDatabase::updateDictionaryEntriesFromDB(DictionaryDatabase* sourceDB)
{
    // Check if we are already up to date
    QSqlQuery versionCheckQuery(mSqlDatabase);
    versionCheckQuery.prepare("select version_id from versions where version_id = ?");
    versionCheckQuery.addBindValue(GApp->DICTIONARY_DB_VERSION);
    versionCheckQuery.exec();
    while (versionCheckQuery.next())
    {
        return true; // already up to date!
    }

    // Clear dictionary entries
    QSqlQuery deleteQuery(mSqlDatabase);
    deleteQuery.exec("delete from dict_entries");

    bool failed = false;

    QSqlQuery readQuery(sourceDB->getSqlDatabase());
    QSqlQuery writeQuery(mSqlDatabase);

    bool readResult = readQuery.exec("select id, kanji_string, kana_string, romaji_string, trans_string from dict_entries");
    if(readResult)
    {
        mSqlDatabase.transaction();
        writeQuery.prepare("insert into dict_entries (id, kanji_string, kana_string, romaji_string, trans_string) values (:id, :kanji_string, :kana_string, :romaji_string, :trans_string)");

        while(readQuery.next())
        {
            if(readQuery.isValid())
            {
                int id = readQuery.value(0).toInt();
                QString kanji = readQuery.value(1).toString();
                QString kana = readQuery.value(2).toString();
                QString romaji = readQuery.value(3).toString();
                QString trans = readQuery.value(4).toString();

                writeQuery.bindValue(":id", id);
                writeQuery.bindValue(":kanji_string", kanji);
                writeQuery.bindValue(":kana_string", kana);
                writeQuery.bindValue(":romaji_string", romaji);
                writeQuery.bindValue(":trans_string", trans);

                bool writeStatus = writeQuery.exec();
                if(!writeStatus)
                {
                    qDebug() << "Write failed";
                    mSqlDatabase.rollback();
                    failed = true;
                    break;
                }
            }
            else
            {
                qDebug() << "Invalid read query";
                mSqlDatabase.rollback();
                failed = true;
                break;
            }
        }
        bool writeStatus = mSqlDatabase.commit();
        qDebug() << Q_FUNC_INFO << "commit:" << writeStatus;
        //mSqlDatabase.close();
    }
    else
    {
        qDebug() << "Failed to read dict entries form db";
        mSqlDatabase.rollback();
        failed = true;
    }

    if(!failed)
    {
        QSqlQuery versionUpdateQuery(mSqlDatabase);
        versionUpdateQuery.prepare("insert into versions values(?)");
        versionUpdateQuery.addBindValue(GApp->DICTIONARY_DB_VERSION);
        versionUpdateQuery.exec();
    }

    return !failed;
}

void UserDatabase::addHistoryEntry(DictionaryEntry* entry)
{
    QDateTime timestamp = QDateTime::currentDateTime();

    QSqlQuery deleteQuery(mSqlDatabase);
    deleteQuery.prepare("delete from history where dict_entry = ?");
    deleteQuery.addBindValue(entry->getID());
    deleteQuery.exec();

    QSqlQuery insertQuery(mSqlDatabase);
    insertQuery.prepare("insert into history(dict_entry, time) values(?,?)");
    insertQuery.addBindValue(entry->getID());
    insertQuery.addBindValue(timestamp);
    insertQuery.exec();
}

void UserDatabase::addToFavourites(DictionaryEntry* entry)
{
    QDateTime timestamp = QDateTime::currentDateTime();

    QSqlQuery favCollQuery(mSqlDatabase);
    favCollQuery.prepare("insert into saved_items(dict_entry, coll_id, time) values(?, ?, ?)");
    favCollQuery.addBindValue(entry->getID());
    favCollQuery.addBindValue(1);
    favCollQuery.addBindValue(timestamp);
    favCollQuery.exec();
}

void UserDatabase::removeFromFavourites(DictionaryEntry* entry)
{
    QSqlQuery deleteQuery(mSqlDatabase);
    deleteQuery.prepare("delete from saved_items where dict_entry = ?");
    deleteQuery.addBindValue(entry->getID());
    deleteQuery.exec();
}

bool UserDatabase::isFavourited(DictionaryEntry* entry)
{
    QSqlQuery selectQuery(mSqlDatabase);
    selectQuery.prepare("select * from saved_items where dict_entry = ?");
    selectQuery.addBindValue(entry->getID());
    selectQuery.exec();
    while(selectQuery.next())
        return true;
    return false;
}

void UserDatabase::getFavourites(QList<DictionaryEntry*>& outDictEntries)
{
    QSqlQuery searchQuery(mSqlDatabase);
    searchQuery.exec("select dict_entries.id, dict_entries.kanji_string, dict_entries.kana_string, dict_entries.romaji_string, dict_entries.trans_string from saved_items left join dict_entries on saved_items.dict_entry = dict_entries.id group by dict_entries.id order by saved_items.time");

    //outDictEntries.reserve(searchQuery.size());

    int i = 1000000; // TODO: Store priority in db?
    while (searchQuery.next())
    {
        int id = searchQuery.value(0).toInt();
        QString kanjiString = searchQuery.value(1).toString();
        QString kanaString = searchQuery.value(2).toString();
        QString romajiString = searchQuery.value(3).toString();
        QString transString = searchQuery.value(4).toString();
        QStringList transList = transString.split(';');

        DictionaryEntry* dictEntry = new DictionaryEntry(id, kanjiString, kanaString, romajiString, transList, i);
        outDictEntries.push_back(dictEntry);

        i--;
    }
}

void UserDatabase::loadHistory(QList<DictionaryEntry*>& outDictEntries)
{
    QSqlQuery searchQuery(mSqlDatabase);
    searchQuery.exec("select dict_entries.id, dict_entries.kanji_string, dict_entries.kana_string, dict_entries.romaji_string, dict_entries.trans_string from history left join dict_entries on history.dict_entry = dict_entries.id group by dict_entries.id order by history.time");

    //outDictEntries.reserve(searchQuery.size());

    int i = 1000000; // TODO: Store priority in db?
    while (searchQuery.next())
    {
        int id = searchQuery.value(0).toInt();
        QString kanjiString = searchQuery.value(1).toString();
        QString kanaString = searchQuery.value(2).toString();
        QString romajiString = searchQuery.value(3).toString();
        QString transString = searchQuery.value(4).toString();
        QStringList transList = transString.split(';');

        DictionaryEntry* dictEntry = new DictionaryEntry(id, kanjiString, kanaString, romajiString, transList, i);
        outDictEntries.push_back(dictEntry);

        i--;
    }
}
