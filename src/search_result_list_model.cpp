#include "search_result_list_model.h"
#include <QVariant>
#include <QModelIndex>

SearchResultListModel::SearchResultListModel(QObject *parent)
{
}

SearchResultListModel::~SearchResultListModel()
{
    mSearchResults.clear();
}

int SearchResultListModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : mSearchResults.size();
}

QVariant SearchResultListModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid() && (index.row() >= 0 || index.row() < mSearchResults.count()))
        return QVariant::fromValue((QObject*)mSearchResults[index.row()]);

    return QVariant();
}

void SearchResultListModel::addSearchResult(DictionaryEntry* item)
{
    beginInsertRows(QModelIndex(), mSearchResults.size(), mSearchResults.size());
    mSearchResults.push_back(item);
    endInsertRows();

    sizeChanged(size());
}

void SearchResultListModel::clearSearchResults()
{
    beginResetModel();
    mSearchResults.clear();
    endResetModel();
    sizeChanged(size());
}
