/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QString>
#include <unordered_map>
#include <string>
#include "dictionary_entry.h"

/*
 * XML Dictionary Loader.
 * Loads dictionary entries from an XML file.
 */
class XmlDictLoader
{
private:
    std::unordered_map<std::string, int> mPriorityValues;
    int parsePriority(QString priString);
    bool parseKanji(QDomElement elem, QStringList& outList, int& priority);
    bool parseReading(QDomElement elem, QStringList& outList, int& priority);
    bool parseTranslation(QDomElement elem, QStringList& outList);
    DictionaryEntry* parseEntry(QDomElement entry);

public:
    XmlDictLoader();
    ~XmlDictLoader();

    /*
     * Load dictionary entries form XML.
     * The loaded entries will be stored in "outDictEntries"
     */
    void laodFromXML(QString xmlPath, QList<DictionaryEntry*>& outDictEntries);
};
