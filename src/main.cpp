/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QString>
#include <QQuickView>
#include <QQmlContext>
#include <QUrl>
#include "app.h"

// Define this if you want to re-build the dictionary database:
//#define WAKARAN_DATABASE_SETUP

#ifdef WAKARAN_DATABASE_SETUP
int main(int argc, char *argv[])
{
    GApp = new App();

    QList<DictionaryEntry*> dictEntries;

    QString xmlPath = ":/JMdict_e"; // MODIFY THIS TO YOUR DESIRED PATH

    XmlDictLoader xmlDictLoader;
    xmlDictLoader.laodFromXML(xmlPath, dictEntries);
    GApp->mDictionaryDatabase = new DictionaryDatabase();
    GApp->mDictionaryDatabase->loadDatabase("dict.sqlite");
    GApp->mDictionaryDatabase->importDictionaryEntries(dictEntries);
}
#else
int main(int argc, char *argv[])
{
    qDebug() << "Starting app from main.cpp";

    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("wakaran-dictionary");

    GApp = new App();
    GApp->initialiseApp();

    // Create view
    QQuickView *view = new QQuickView();
    view->rootContext()->setContextProperty("mainViewModel", GApp->mMainViewModel);
    view->rootContext()->setContextProperty("searchPageModel", GApp->mSearchPageViewModel);
    view->rootContext()->setContextProperty("historyPageModel", GApp->mHistoryListModel);
    view->rootContext()->setContextProperty("searchResultListModel", GApp->mSearchResultListModel);
    view->rootContext()->setContextProperty("dictEntryPageViewModel", GApp->mDictEntryPageViewModel);
    view->rootContext()->setContextProperty("historyListModel", GApp->mHistoryListModel);
    view->rootContext()->setContextProperty("favouritesListModel", GApp->mFavouritesListModel);
    view->setSource(QUrl("qrc:/Main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
#endif
