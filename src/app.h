/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WAKARAN_APP_H
#define WAKARAN_APP_H

#include "main_view_model.h"
#include "dictionary_entry.h"
#include "search_page_view_model.h"
#include "search_result_list_model.h"
#include "dict_entry_page_view_model.h".h"
#include "history_page_view_model.h"
#include "favourites_page_view_model.h"

class DictionaryDatabase;
class UserDatabase;

class App
{
private:
    void initialiseDatabase();

public:
    DictionaryDatabase* mBaseEntryDatabase;
    UserDatabase* mUserDatabase;
    DictionaryDatabase* mDictionaryDatabase;

    MainViewModel* mMainViewModel;
    SearchPageViewModel* mSearchPageViewModel;
    DictEntryPageViewModel* mDictEntryPageViewModel;
    HistoryPageViewModel* mHistoryPageViewModel;
    FavouritesPageViewModel* mFavouritesPageViewModel;

    SearchResultListModel* mSearchResultListModel;
    SearchResultListModel* mHistoryListModel;
    SearchResultListModel* mFavouritesListModel;

    const int DICTIONARY_DB_VERSION = 1;

    bool initialiseApp();
};

extern App* GApp;

#endif
