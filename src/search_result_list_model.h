﻿#ifndef SEARCHRESULTLISTMODEL_H
#define SEARCHRESULTLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "dictionary_entry.h"

class SearchResultListModel : public QAbstractListModel
{
    Q_OBJECT

private:
    QList<DictionaryEntry*> mSearchResults;

public:
    explicit SearchResultListModel(QObject *parent = 0);
    ~SearchResultListModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void addSearchResult(DictionaryEntry* item);
    void clearSearchResults();
    int size() const { return mSearchResults.size(); }

signals:
    void sizeChanged(int arg);
};

#endif // SEARCHRESULTLISTMODEL_H
