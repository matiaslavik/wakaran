﻿#ifndef DICTIONARYENTRY_H
#define DICTIONARYENTRY_H

#include <QObject>
#include <QString>

/*
 * Dictionary entry class.
 * Contains all the writings/readings/transations for a single dictionary entry.
 */
class DictionaryEntry : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString sourceText READ getSourceText NOTIFY entryChangedEvent)
    Q_PROPERTY(QString hiraganaText READ getHiraganaText NOTIFY entryChangedEvent)
    Q_PROPERTY(QString romajiText READ getRomajiText NOTIFY entryChangedEvent)
    Q_PROPERTY(QStringList translations READ getTranslations NOTIFY entryChangedEvent)

private:
    QString mSourceText;
    QString mHiraganaText;
    QString mRomajiText;
    QStringList mTranslations;
    int mPriority = 0;
    int mID = 0;

public:
    DictionaryEntry();

    DictionaryEntry(int id, QString source, QString hiragana, QString romaji, QStringList translations, int priority);

    QString getSourceText() const { return mSourceText; }
    QString getHiraganaText() const { return mHiraganaText; }
    QString getRomajiText() const { return mRomajiText; }
    QStringList getTranslations() const { return mTranslations; }
    int getPriority() const { return mPriority; }
    int getID() const { return mID; }

signals:
    void entryChangedEvent();
};

#endif
