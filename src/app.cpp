/*
 * Copyright (C) 2020  Matias Lavik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * wakaran is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"
#include <QStandardPaths>
#include <QDir>
#include "dictionary_database.h"
#include "user_database.h"

App* GApp = nullptr;

void App::initialiseDatabase()
{
    // Load dictionary database (read-only DB containing dictionary entries)
    mBaseEntryDatabase = new DictionaryDatabase();
    mBaseEntryDatabase->loadDatabase(QString("assets/") + QString("dict.sqlite"), "dict_db");
    //mBaseEntryDatabase->constructDictionaryEntries(mSearchPageViewModel->mDictionaryEntries);

    // Create user database (read-write database for user settings + copy of dict.sqlite's dictionary entries)
    QString dbLoc = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString dbFullPath = QDir(dbLoc).filePath("userdb.sqlite");
    mUserDatabase = new UserDatabase();
    bool userDBRes = mUserDatabase->loadDatabase(dbFullPath, "user_db");
    if(userDBRes)
    {
        // Copy dictionary entries from the dictionary database.
        // Any new entries (after an update to Wakaran) will be added to the user database.
        // If we are already up-to-date, nothing will happen.
        if(mUserDatabase->updateDictionaryEntriesFromDB(mBaseEntryDatabase))
        {
            mDictionaryDatabase = mUserDatabase;
        }
        else
        {
            qDebug() << "Failed to copy dictionary entries to user database.";
            mDictionaryDatabase = mBaseEntryDatabase; // user read-only dictionary DB for searching
        }
    }
    else
    {
        qDebug() << "Failed to initialise user database. History and collections will not work.";
        delete mUserDatabase;
        mUserDatabase = nullptr;
        mDictionaryDatabase = mBaseEntryDatabase; // user read-only dictionary DB for searching
    }
}

bool App::initialiseApp()
{
    // Create appdata dir (if it doesn't exist)
    if(!QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).exists())
        QDir().mkdir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));

    // Create view models
    mMainViewModel = new MainViewModel();
    mSearchPageViewModel = new SearchPageViewModel();
    mHistoryPageViewModel = new HistoryPageViewModel();
    mDictEntryPageViewModel = new DictEntryPageViewModel();
    mSearchResultListModel = new SearchResultListModel();
    mHistoryListModel = new SearchResultListModel();
    mFavouritesListModel = new SearchResultListModel();
    mFavouritesPageViewModel = new FavouritesPageViewModel();

    initialiseDatabase();

    // TODO: fix this
    mHistoryPageViewModel->initialise();
    mFavouritesPageViewModel->initialise();
}
